# Summary

This project was created to

* Have a play ground for technologies
* Create an app that motivates me learning by checking in every day
* Have some sample usage for different technologies

# Used technologies

Technologie | Version | Link
------------| --------| ---------
Gradle KTS | - | https://docs.gradle.org/current/userguide/kotlin_dsl.html
Jetpack's Room | `2.2.5` | https://developer.android.com/jetpack/androidx/releases/room
Jetpack's Navigation | `2.3.0-alpha04` | https://developer.android.com/guide/navigation
Jetpack's LiveData | - | https://developer.android.com/topic/libraries/architecture/livedata
Dagger | `2.24` | https://developer.android.com/training/dependency-injection/dagger-android
Kotlin Coroutines | | https://developer.android.com/kotlin/coroutines

# Used architecture patterns

* MVI + MVVM (combined as a state + event + effect approach)

# Screenshots

<img src="readmeres/before-check-in.png" alt="Before Check In" width="200"/>
<img src="readmeres/after-check-in.png" alt="After Check In" width="200"/>
<img src="readmeres/start-new-streak.png" alt="Start new streak" width="200"/>
