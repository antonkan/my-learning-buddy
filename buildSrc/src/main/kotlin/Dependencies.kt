const val kotlinVersion = "1.3.70"

object BuildPlugins {

    object Versions {
        const val gradlePluginVersion = "3.6.1"
    }

    const val androidGradlePlugin = "com.android.tools.build:gradle:${Versions.gradlePluginVersion}"
    const val kotlinGradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion"
    const val androidApplication = "com.android.application"
    const val kotlinAndroid = "kotlin-android"
    const val kotlinAndroidExtensions = "kotlin-android-extensions"
    const val kotlinKapt = "kapt"

}

object AndroidSdk {
    const val min = 26
    const val compile = 29
    const val target = compile
}

object Libraries {
    private object Versions {
        const val appcompat = "1.1.0"
        const val constraintLayout = "1.1.3"
        const val ktx = "1.2.0"
        const val room = "2.2.5"
        const val nav = "2.3.0-alpha04"
        const val dagger = "2.24"
    }

    const val kotlinStdLib      = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlinVersion"
    const val appCompat         = "androidx.appcompat:appcompat:${Versions.appcompat}"
    const val constraintLayout  = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    const val ktxCore           = "androidx.core:core-ktx:${Versions.ktx}"
    const val ktxRoom           = "androidx.room:room-ktx:${Versions.room}"
    const val ktxRoomCompiler   = "androidx.room:room-compiler:${Versions.room}"
    const val ktxRoomExtensions = "androidx.room:room-ktx:${Versions.room}"
    const val ktxRoomRxJava     = "androidx.room:room-rxjava2:${Versions.room}"
    const val navFragment       = "androidx.navigation:navigation-fragment:${Versions.nav}"
    const val navFragmentUi     = "androidx.navigation:navigation-ui:${Versions.nav}"
    const val ktxNavFragment    = "androidx.navigation:navigation-fragment-ktx:${Versions.nav}"
    const val ktxNavFragmentUi  = "androidx.navigation:navigation-ui-ktx:${Versions.nav}"
    const val dagger            = "com.google.dagger:dagger-android:${Versions.dagger}"
    const val daggerSupport     = "com.google.dagger:dagger-android-support:${Versions.dagger}"
    const val daggerProcessor   = "com.google.dagger:dagger-android-processor:${Versions.dagger}"
    const val daggerCompiler    = "com.google.dagger:dagger-compiler:${Versions.dagger}"
}

object TestLibraries {
    private object Versions {
        const val junit4 = "4.12"
        const val testRunner = "1.1.0-alpha4"
        const val espresso = "3.2.0"
    }
    const val junit4     = "junit:junit:${Versions.junit4}"
    const val testRunner = "androidx.test:runner:${Versions.testRunner}"
    const val espresso   = "androidx.test.espresso:espresso-core:${Versions.espresso}"
}
