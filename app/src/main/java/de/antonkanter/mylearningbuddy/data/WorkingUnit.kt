package de.antonkanter.mylearningbuddy.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class WorkingUnit(
    var date: String,
    var durationInMin: Int,
    var streakCounter: Int
) {
    @PrimaryKey(autoGenerate = true) var id: Int = 0
}
