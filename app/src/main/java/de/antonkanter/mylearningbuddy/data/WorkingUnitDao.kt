package de.antonkanter.mylearningbuddy.data

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface WorkingUnitDao {
    @Insert
    suspend fun insertWorkingUnit(workingUnit: WorkingUnit)

    @Delete
    suspend fun removeWorkingUnit(workingUnit: WorkingUnit)

    @Query("SELECT * FROM WorkingUnit WHERE date = :date")
    suspend fun getWorkingUnitsByDate(date: String): List<WorkingUnit>

    @Query("SELECT MAX(streakCounter) FROM WorkingUnit")
    suspend fun getMaxStreakCounter(): Int?
}
