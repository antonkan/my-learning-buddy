package de.antonkanter.mylearningbuddy.data

import javax.inject.Inject

class WorkingUnitRepo @Inject constructor(database: MlbDatabase) {

    private val dao: WorkingUnitDao = database.workingUnitDao()

    suspend fun getWorkingUnitsByDate(date: String) = dao.getWorkingUnitsByDate(date)

    suspend fun addWorkingUnit(workingUnit: WorkingUnit) = dao.insertWorkingUnit(workingUnit)

    suspend fun removeWorkingUnit(workingUnit: WorkingUnit) = dao.removeWorkingUnit(workingUnit)

    suspend fun getStreakHighscore() = dao.getMaxStreakCounter()
}
