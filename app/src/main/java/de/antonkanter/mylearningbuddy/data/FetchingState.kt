package de.antonkanter.mylearningbuddy.data

sealed class FetchingState {
    object Success : FetchingState()
    object Loading : FetchingState()
    data class Error(val throwable: Throwable) : FetchingState()
}
