package de.antonkanter.mylearningbuddy.data

data class ButtonState(
    val text: String,
    val visibility: ButtonVisibility
)

enum class ButtonVisibility {
    Active, Disabled, Hidden
}
