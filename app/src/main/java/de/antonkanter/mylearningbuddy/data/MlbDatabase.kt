package de.antonkanter.mylearningbuddy.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

@Database(entities = [WorkingUnit::class], version = 2, exportSchema = false)
abstract class MlbDatabase : RoomDatabase() {

    abstract fun workingUnitDao(): WorkingUnitDao

    companion object {
        @Volatile
        private var INSTANCE: MlbDatabase? = null

        private val MIGRATION_1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE WorkingUnit ADD COLUMN streakCounter INTEGER NOT NULL DEFAULT 'null'")
                database.execSQL("UPDATE WorkingUnit SET streakCounter = 1")
            }
        }


        fun getDatabase(context: Context): MlbDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    MlbDatabase::class.java,
                    "mlb_database"
                )
                    .addMigrations(MIGRATION_1_2)
                    .build()
                INSTANCE = instance
                return instance
            }
        }
    }
}
