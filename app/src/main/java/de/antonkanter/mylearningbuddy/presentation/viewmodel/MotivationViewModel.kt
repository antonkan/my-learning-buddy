package de.antonkanter.mylearningbuddy.presentation.viewmodel

import androidx.lifecycle.viewModelScope
import de.antonkanter.mylearningbuddy.data.WorkingUnit
import de.antonkanter.mylearningbuddy.data.WorkingUnitRepo
import de.antonkanter.mylearningbuddy.presentation.base.MviViewModel
import de.antonkanter.mylearningbuddy.presentation.model.MotivationModel
import de.antonkanter.mylearningbuddy.utils.MlbTimeUtils
import kotlinx.coroutines.launch
import javax.inject.Inject

class MotivationViewModel @Inject constructor(
    private val workingUnitRepo: WorkingUnitRepo
) : MviViewModel<MotivationState, MotivationEvent, MotivationEffect>() {

    private val model = MotivationModel { viewState = it }

    init {
        viewState = model.state
    }

    override fun processEvent(viewEvent: MotivationEvent) {
        when (viewEvent) {
            MotivationEvent.OnWelcomePageShown -> onWelcomePageShown()
            MotivationEvent.CheckInButtonClicked -> checkIn()
            MotivationEvent.RevertCheckInButtonClicked -> revertCheckIn()
        }
    }

    private fun onWelcomePageShown() {
        viewModelScope.launch {
            fetchCheckInStatus()
        }
    }

    private fun checkIn() {
        viewModelScope.launch {
            addWorkingUnit()
            fetchCheckInStatus()
        }
    }

    private fun revertCheckIn() {
        viewModelScope.launch {
            removeTodayWorkingUnit()
            fetchCheckInStatus()
        }
    }

    private suspend fun addWorkingUnit() {
        val currentCounter = getStreakCounter(MlbTimeUtils.yesterdayAsDateString)
        workingUnitRepo.addWorkingUnit(
            WorkingUnit(
                date = MlbTimeUtils.nowAsDateString,
                durationInMin = 0,
                streakCounter = currentCounter + 1
            )
        )
    }

    private suspend fun removeTodayWorkingUnit() {
        workingUnitRepo.getWorkingUnitsByDate(MlbTimeUtils.nowAsDateString)
            .forEach { workingUnitRepo.removeWorkingUnit(it) }
    }

    private suspend fun fetchCheckInStatus() {
        model.setLoading()
        val highscore = getCurrentHighscore() ?: 0
        val todayCounter = getStreakCounter(MlbTimeUtils.nowAsDateString)
        when {
            todayCounter > 0 -> model.setSuccess(true, todayCounter, highscore)
            else -> {
                val yesterdayCounter = getStreakCounter(MlbTimeUtils.yesterdayAsDateString)
                model.setSuccess(false, yesterdayCounter, highscore)
            }
        }
    }

    private suspend fun getStreakCounter(date: String) =
        workingUnitRepo.getWorkingUnitsByDate(date).lastOrNull()?.streakCounter ?: 0

    private suspend fun getCurrentHighscore() =
        workingUnitRepo.getStreakHighscore()
}
