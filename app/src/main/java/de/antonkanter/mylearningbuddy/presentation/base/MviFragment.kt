package de.antonkanter.mylearningbuddy.presentation.base

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.annotation.MainThread
import androidx.fragment.app.createViewModelLazy
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer

abstract class MviFragment<STATE, EFFECT, EVENT, VM : MviViewModel<STATE, EVENT, EFFECT>>(
    @LayoutRes layoutRes: Int
) : DiFragment(layoutRes) {
    abstract val viewModelFactory: ViewModelProvider.Factory
    abstract val viewModel: VM

    private lateinit var effectDisposable: Disposable

    private val viewStateObserver = Observer<STATE> {
        renderViewState(it)
    }

    private val viewEffectConsumer = Consumer<EFFECT> {
        renderViewEffect(it)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.viewStates.observe(viewLifecycleOwner, viewStateObserver)
        effectDisposable = viewModel.viewEffects.subscribe(viewEffectConsumer)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        effectDisposable.dispose()
    }

    fun processViewEvent(event: EVENT) {
        viewModel.processEvent(event)
    }

    abstract fun renderViewState(viewState: STATE)
    abstract fun renderViewEffect(viewEffect: EFFECT)

    @MainThread
    protected inline fun <reified VM : ViewModel> lazyVm() =
        createViewModelLazy(VM::class, { viewModelStore }, { viewModelFactory })
}
