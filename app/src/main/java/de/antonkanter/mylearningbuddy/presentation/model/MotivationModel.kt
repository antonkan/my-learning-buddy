package de.antonkanter.mylearningbuddy.presentation.model

import de.antonkanter.mylearningbuddy.presentation.viewmodel.CheckInButtonState
import de.antonkanter.mylearningbuddy.presentation.viewmodel.MotivationState
import kotlin.properties.Delegates

class MotivationModel(
    val onStateChanged: (MotivationState) -> Unit
) {
    private val subtitleTextLoading = "Let's see what I can do for you"
    private val subtitleTextCheckedIn = "Nice! You checked in for today!"
    private val subtitleTextNotCheckedIn = "Awesome! Come on, check in"

    private val initialState = MotivationState(
        titleText = "Welcome back!",
        subtitleText = subtitleTextLoading,
        checkInButtonState = CheckInButtonState.Pending
    )

    private var _state: MotivationState by Delegates.observable(initialState) { _, _, newValue ->
        onStateChanged(newValue)
    }
    val state: MotivationState
        get() = _state

    fun setLoading() {
        _state = _state.copy(
            subtitleText = subtitleTextLoading,
            checkInButtonState = CheckInButtonState.Pending
        )
    }

    fun setSuccess(
        hasCheckedInToday: Boolean,
        checkedInCounter: Int,
        currentHighscore: Int
    ) {
        val newButtonState = when {
            hasCheckedInToday -> CheckInButtonState.Success
            else -> CheckInButtonState.Active
        }
        val newSubtitleText = when {
            hasCheckedInToday -> subtitleTextCheckedIn
            else -> subtitleTextNotCheckedIn
        }
        val counterText = generateCounterDescriptionText(hasCheckedInToday, checkedInCounter)
        _state = _state.copy(
            subtitleText = newSubtitleText,
            highscore = currentHighscore,
            checkInButtonState = newButtonState,
            counterText = counterText,
            counter = checkedInCounter
        )
    }

    private fun generateCounterDescriptionText(checkedIn: Boolean, counter: Int) =
        when {
            checkedIn -> "This makes it"
            counter > 0 -> "and get your current streak to ${counter + 1}!"
            else -> "and start a new streak!"
        }
}
