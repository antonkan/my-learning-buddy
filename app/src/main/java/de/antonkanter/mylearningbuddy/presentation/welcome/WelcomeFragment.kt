package de.antonkanter.mylearningbuddy.presentation.welcome

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import de.antonkanter.mylearningbuddy.R
import de.antonkanter.mylearningbuddy.presentation.base.MviFragment
import de.antonkanter.mylearningbuddy.presentation.viewmodel.*
import kotlinx.android.synthetic.main.fragment_welcome.*
import javax.inject.Inject

class WelcomeFragment
    : MviFragment<MotivationState, MotivationEffect, MotivationEvent, MotivationViewModel>(
    R.layout.fragment_welcome
) {

    @Inject
    internal lateinit var vmFactory: ViewModelProvider.Factory
    override val viewModelFactory: ViewModelProvider.Factory
        get() = vmFactory

    private val _viewModel: MotivationViewModel by lazyVm()
    override val viewModel: MotivationViewModel
        get() = _viewModel

    override fun renderViewState(viewState: MotivationState) {
        welcome_title_text.text = viewState.titleText
        welcome_highscore_counter.text = viewState.highscore.toString()
        welcome_subtitle_text.text = viewState.subtitleText
        welcome_counter_text.renderCounterDescriptionText(viewState)
        check_in_button.renderButton(viewState)
        check_in_counter.renderCounterImageText(viewState)
        check_in_revert_button.renderRevertButton(viewState)
    }

    override fun renderViewEffect(viewEffect: MotivationEffect) {
        TODO("Not yet implemented")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        processViewEvent(MotivationEvent.OnWelcomePageShown)

        check_in_button.setOnClickListener {
            processViewEvent(MotivationEvent.CheckInButtonClicked)
        }
        check_in_revert_button.setOnClickListener {
            processViewEvent(MotivationEvent.RevertCheckInButtonClicked)
        }
    }

    private fun TextView.renderCounterDescriptionText(viewState: MotivationState) {
        viewState.counterText?.let {
            isVisible = true
            text = it
        } ?: run {
            isVisible = false
        }
    }

    private fun ImageView.renderButton(viewState: MotivationState) {
        val (drawableRes, enabled) = when (viewState.checkInButtonState) {
            CheckInButtonState.Active -> R.drawable.ic_check_in_cta to true
            CheckInButtonState.Success -> R.drawable.ic_check_in_success to false
            CheckInButtonState.Pending -> R.drawable.ic_check_in_cta to false
        }
        setImageResource(drawableRes)
        isEnabled = enabled
    }

    private fun TextView.renderCounterImageText(viewState: MotivationState) {
        when (viewState.checkInButtonState) {
            CheckInButtonState.Success -> {
                isVisible = true
                text = viewState.counter.toString()
            }
            else -> {
                isVisible = false
            }
        }
    }

    private fun ImageView.renderRevertButton(viewState: MotivationState) {
        isVisible = viewState.checkInButtonState == CheckInButtonState.Success
    }
}
