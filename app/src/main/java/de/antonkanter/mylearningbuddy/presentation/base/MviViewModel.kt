package de.antonkanter.mylearningbuddy.presentation.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.subjects.PublishSubject

abstract class MviViewModel<STATE, EVENT, EFFECT> : ViewModel() {

    private val _viewStates: MutableLiveData<STATE> = MutableLiveData()
    val viewStates: LiveData<STATE> = _viewStates

    private var _viewState: STATE? = null
    protected var viewState: STATE
        get() = _viewState
            ?: throw UninitializedPropertyAccessException("\"viewState\" was accessed before being initialized")
        set(value) {
            _viewState = value
            _viewStates.postValue(value)
        }

    val viewEffects: PublishSubject<EFFECT> = PublishSubject.create()

    private var _viewEffect: EFFECT? = null
    protected var viewEffect: EFFECT
        get() = _viewEffect
            ?: throw UninitializedPropertyAccessException("\"viewEffect\" was accessed before being initialized")
        set(value) {
            _viewEffect = value
            viewEffects.onNext(value)
        }

    abstract fun processEvent(viewEvent: EVENT)
}
