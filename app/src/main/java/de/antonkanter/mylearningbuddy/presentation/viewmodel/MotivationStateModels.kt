package de.antonkanter.mylearningbuddy.presentation.viewmodel

data class MotivationState(
    val titleText: String,
    val subtitleText: String,
    val checkInButtonState: CheckInButtonState,
    val counterText: String? = null,
    val counter: Int = -1,
    val highscore: Int = 0
)

sealed class MotivationEvent {
    object OnWelcomePageShown : MotivationEvent()
    object CheckInButtonClicked : MotivationEvent()
    object RevertCheckInButtonClicked : MotivationEvent()
}

sealed class MotivationEffect {
    data class ShowToast(val message: String) : MotivationEffect()
}

enum class CheckInButtonState {
    Active, Pending, Success
}
