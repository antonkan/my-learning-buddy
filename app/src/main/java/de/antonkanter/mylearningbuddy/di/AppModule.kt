package de.antonkanter.mylearningbuddy.di

import android.content.Context
import dagger.Module
import dagger.Provides
import de.antonkanter.mylearningbuddy.Application

@Module
class AppModule {
    @Provides
    fun provideContext(application: Application): Context {
        return application.applicationContext
    }
}
