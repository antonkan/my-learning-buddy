package de.antonkanter.mylearningbuddy.di

import android.content.Context
import dagger.Module
import dagger.Provides
import de.antonkanter.mylearningbuddy.data.MlbDatabase
import de.antonkanter.mylearningbuddy.data.WorkingUnitRepo

@Module
class DatabaseModule {
    @Provides
    fun provideWorkingUnitRepo(context: Context): WorkingUnitRepo {
        return WorkingUnitRepo(MlbDatabase.getDatabase(context))
    }
}
