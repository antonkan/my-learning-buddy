package de.antonkanter.mylearningbuddy.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import de.antonkanter.mylearningbuddy.presentation.welcome.WelcomeFragment

@Module
abstract class FragmentBuilderModule {
    @ContributesAndroidInjector
    abstract fun contributeWelcomeFragment(): WelcomeFragment
}
