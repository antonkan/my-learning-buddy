package de.antonkanter.mylearningbuddy.di

import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import de.antonkanter.mylearningbuddy.Application
import javax.inject.Singleton

@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        FragmentBuilderModule::class,
        ActivityBuilderModule::class,
        ViewModelModule::class,
        DatabaseModule::class
    ]
)
@Singleton
interface AppComponent : AndroidInjector<Application> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<Application>()
}

