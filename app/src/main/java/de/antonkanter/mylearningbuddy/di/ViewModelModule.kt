package de.antonkanter.mylearningbuddy.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import de.antonkanter.mylearningbuddy.presentation.viewmodel.MotivationViewModel

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(MotivationViewModel::class)
    abstract fun bindMotivationViewModel(
        viewModel: MotivationViewModel
    ) : ViewModel

    @Binds
    abstract fun bindViewModelFactory(
        factory: ViewModelProviderFactory
    ): ViewModelProvider.Factory
}
