package de.antonkanter.mylearningbuddy.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import de.antonkanter.mylearningbuddy.MainActivity

@Module
abstract class ActivityBuilderModule {
    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity
}
