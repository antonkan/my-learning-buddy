package de.antonkanter.mylearningbuddy

import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import de.antonkanter.mylearningbuddy.di.DaggerAppComponent

class Application : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().create(this)
    }
}
