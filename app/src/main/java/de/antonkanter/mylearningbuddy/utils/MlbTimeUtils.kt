package de.antonkanter.mylearningbuddy.utils

import java.text.SimpleDateFormat
import java.util.*

@Suppress("MemberVisibilityCanBePrivate")
object MlbTimeUtils {
    private const val DATE_FORMAT = "dd/MM/yyyy"
    private val sdf = SimpleDateFormat(DATE_FORMAT, Locale.GERMAN)

    val nowInMillis: Long
        get() = Calendar.getInstance().timeInMillis

    val yesterdayInMillis: Long
        get() = Calendar.getInstance().let {
            it.add(Calendar.DAY_OF_MONTH, -1)
            it.timeInMillis
        }

    fun Long.getDateString(): String = sdf.format(Date(this))

    val nowAsDateString: String
        get() = nowInMillis.getDateString()

    val yesterdayAsDateString: String
        get() = yesterdayInMillis.getDateString()
}
